
function fish_greeting
end

function get_scm_text
    set -l d (date)
    set -l git (__fish_git_prompt "%s"| tr -d '|')

    if test -n "$git"
        set git (string replace "CHERRY-PICKING" " "\ue29b" " $git)
        set git (string replace "MERGING" " "\uf419" " $git)
        set -l num_stashed (count (git stash list))
#       set -l upstream (git status --branch --porcelain=v2 | grep upstream | awk '{print $3}' | tr -d "[:blank:]")
        if test $num_stashed -gt "0"
            set git "$git"(set_color FF5800; echo -n "⚑$num_stashed"; set_color normal)
        end
#       if test -n "$upstream"
#           set git (set_color B6B6F6; echo -n "$upstream <-> "; set_color normal)"$git"
#       end
    end

    echo "$git"
end

function fish_prompt --description 'Write out the prompt'

    set -l last_status $status
    set -l scm (get_scm_text)
    #echo $scm
    set -l this_host (set_color 116F87; hostname; set_color normal)
    set -l path (set_color 5E8D87; prompt_pwd; set_color normal)
    set -l vi (vi_mode)

    printf "[$this_host➤ $path"
    if [ $scm ]
        printf " $scm"
    end
    printf "]\n"

    if not test $last_status -eq 0
        set_color $fish_color_error
    end

    echo -n "$vi → "
    set_color normal
end

function vi_mode
    if test "$fish_key_bindings" = "fish_vi_key_bindings"
    or test "$fish_key_bindings" = "fish_hybrid_key_bindings"
        switch $fish_bind_mode
            case default
                set_color red
                echo -n 'N'
            case insert
                set_color green
                echo -n 'I'
            case replace-one
                set_color green
                echo -n 'R'
            case visual
                set_color magenta
                echo -n 'V'
        end
        set_color normal
    end
end

function get_left_prompt
    # PWD
    echo -n (vi_mode)
    echo -n (prompt_pwd)
end
