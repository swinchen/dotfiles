# Base16 Shell
if status --is-interactive
    eval sh $HOME/.base16-shell/scripts/base16-summerfruit-dark.sh
end

set __fish_git_prompt_show_informative_status "yes"
set __fish_git_prompt_showcolorhints "yes"
set fish_prompt_pwd_dir_length 0

function fish_greeting
end

function fish_mode_prompt
end

function rmkh
  sed -i {$argv}"d" {$HOME}"/.ssh/known_hosts"
end

for script in {$HOME}/.dotscripts/fish/*.fish
    source $script
end

fish_add_path -pP "/home/swinchen/.rustup/toolchains/stable-x86_64-unknown-linux-gnu/bin/"

alias config '/usr/bin/git --git-dir={$HOME}/.cfg --work-tree={$HOME}'
alias swift-build "docker run -it --rm -e USERNAME=$USER -v $HOME/Projects:/work --dns-search secure.tethers.com docker.secure.tethers.com/swift/swift-build"
alias petalinux "docker run -it --rm -e USERNAME=$USER -v $HOME/Projects:/work docker.secure.tethers.com/xilinx/petalinux"
alias raven-toolkit "docker run -it --rm --net=host -e XILINXD_LICENSE_FILE=2100@xildev -e DISPLAY=$DISPLAY -e USERNAME=$USER -u $(id -u):$(id -g) -v $HOME/Projects:/work -v $(dirname $SSH_AUTH_SOCK):$(dirname $SSH_AUTH_SOCK) -e SSH_AUTH_SOCK=$SSH_AUTH_SOCK --dns-search secure.tethers.com docker.secure.tethers.com/raven/raven-toolkit"
alias vim 'hx'
