-- packer boilerplate and other good ideas shamelessly stolen from LunarVim:
-- https://github.com/ChristianChiarulli/LunarVim
local install_path = vim.fn.stdpath("data") .. "/site/pack/packer/start/packer.nvim"

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  vim.api.nvim_command("!git clone https://github.com/wbthomason/packer.nvim " .. install_path)
  vim.api.nvim_command("packadd packer.nvim")
end

local packer_ok, packer = pcall(require, "packer")
if not packer_ok then
  return
end

packer.init {
  git = { clone_timeout = 300 },
  display = {
    open_fn = function()
      return require("packer.util").float { border = "single" }
    end,
  },
}


vim.cmd "autocmd BufWritePost plugins.lua PackerCompile"

return require("packer").startup(function(use)
  -- Collection of common configurations for the Nvim LSP client
  use {
    "neovim/nvim-lspconfig",
    config =
      function()
        require('config.lspconfig')
      end,
  }

  -- Completion framework
  use {
    "hrsh7th/nvim-cmp",
    config = function()
      local cmp = require('cmp')
      cmp.setup({
        -- Enable LSP snippets
        snippet = {
          expand = function(args)
            vim.fn["vsnip#anonymous"](args.body)
          end,
        },
        mapping = {
          ['<S-Tab>'] = cmp.mapping.select_prev_item(),
          ['<Tab>'] = cmp.mapping.select_next_item(),
          ['<C-d>'] = cmp.mapping.scroll_docs(-4),
          ['<C-f>'] = cmp.mapping.scroll_docs(4),
          ['<C-Space>'] = cmp.mapping.complete(),
          ['<C-e>'] = cmp.mapping.close(),
          ['<CR>'] = cmp.mapping.confirm({
            behavior = cmp.ConfirmBehavior.Insert,
            select = true,
          })
        },
--
--        -- Installed sources
        sources = {
          { name = 'nvim_lsp' },
          { name = 'vsnip' },
          { name = 'path' },
          { name = 'buffer' },
        },
      })
    vim.opt.completeopt = {'menuone', 'noinsert', 'noselect'}
    vim.opt.shortmess:append("c")
    end,
  }

  -- LSP completion source for nvim-cmp
  use {
    "hrsh7th/cmp-nvim-lsp",
  }

  -- Snippet completion source for nvim-cmp
  use {
    "hrsh7th/cmp-vsnip",
  }

  -- Other use full completion sources
  use {
    "hrsh7th/cmp-path",
  }

  use {
    "hrsh7th/cmp-buffer",
  }

  -- See hrsh7th"s other plugins for more completion sources!

  -- To enable more of the features of rust-analyzer, such as inlay hints and more!
  use {
    "simrat39/rust-tools.nvim",
    config =
      function()
        local opts = {
          tools = { -- rust-tools options
            autoSetHints = true,
            hover_with_actions = true,
            inlay_hints = {
              show_parameter_hints = false,
              parameter_hints_prefix = "",
              other_hints_prefix = "",
            },
          },

          -- all the opts to send to nvim-lspconfig
          -- these override the defaults set by rust-tools.nvim
          -- see https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md#rust_analyzer
          server = {
            -- on_attach is a callback called when the language server attachs to the buffer
            -- on_attach = on_attach,
            settings = {
              -- to enable rust-analyzer settings visit:
              -- https://github.com/rust-analyzer/rust-analyzer/blob/master/docs/user/generated_config.adoc
              ["rust-analyzer"] = {
                -- enable clippy on save
                checkOnSave = {
                  command = "clippy"
                },
              }
            }
          },
        }
        require('rust-tools').setup(opts)
        vim.keymap.set('n', 'ga', vim.lsp.buf.code_action)

        vim.opt.updatetime = 300
        display_diag = function()
          vim.diagnostic.open_float(nil, { focusable = false })
        end

        vim.api.nvim_create_autocmd('CursorHold', { callback=display_diag })
      end,
  }

  -- Snippet engine
  use {
    "hrsh7th/vim-vsnip",
  }

  -- Fuzzy finder
  -- Optional
  use {
    "nvim-lua/popup.nvim",
  }
  use {
    "nvim-lua/plenary.nvim",
  }

  -- Color scheme used in the GIFs!
  -- Plug 'arcticicestudio/nord-vim'


  use {
    "wbthomason/packer.nvim",
  }

  use {
    'chriskempson/base16-vim',
    config = function() vim.cmd('colorscheme base16-summerfruit-dark') end,
  }

  -- A Swiss Army Knife for finding things; a modern ctrlp.vim
  use {
    "nvim-telescope/telescope.nvim",
    requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}},
    config = function()
      require("telescope").setup {
        pickers = {
          tags = {
            ctags_file = ".tags",
          }
        }
      }
      vim.api.nvim_set_keymap('n', '<C-p>', ':Telescope find_files hidden=true<CR>', {noremap = true})
      vim.api.nvim_set_keymap('n', '<leader>pb', ':Telescope buffers<CR>', {noremap = true})
      vim.api.nvim_set_keymap('n', '<leader>pt', ':Telescope tags<CR>', {noremap = true})
    end,
  }

  -- Treesitter support for neovim
  use {
    "nvim-treesitter/nvim-treesitter",
    run = ":TSUpdate",
    config = function()
      require('nvim-treesitter.configs').setup {
        -- one of "all", "maintained" (parsers with maintainers), or a list of languages
        ensure_installed = {"go", "python", "toml", "fish", "c", "rust"},
        highlight = { enable = true },
      }
    end,
  }

  -- vimgrep but fast (because of ripgrep)
  use {
    'mileszs/ack.vim',
    config = function()
      vim.g.ackprg = 'rg --vimgrep --smart-case'
      vim.api.nvim_set_keymap('n', '<leader>*', '*:AckFromSearch!<CR>', {noremap = true})
      vim.api.nvim_set_keymap('n', '<leader>/', ':Ack!<Space>', {noremap = true})
    end
  }

  -- Show diff when editing commit message
  use {
    "rhysd/committia.vim",
  }

  -- Git change status in the left gutter
  use {
    'lewis6991/gitsigns.nvim',
    requires = { 'nvim-lua/plenary.nvim' },
    config = function() require('gitsigns').setup() end,
  }

  -- Explore Vim's history of a file and revert to old versions
  use {
    'mbbill/undotree',
    cmd = 'UndotreeToggle',
    config = function() vim.g.undotree_SetFocusWhenToggle = 1 end,
  }

  use {
    'dense-analysis/ale',
    ft = {'sh', 'bash', 'zsh', 'markdown'},
    config = function() vim.g.ale_python_mypy_options = '--ignore-missing-imports' end
  }

--
--  -- Packer can manage itself
--
--  -- Language Server Protocol configuration
--  use {
--    "neovim/nvim-lspconfig",
--    config = function() require('config.lspconfig') end,
--  }
--
--
--  -- Legacy linting, mainly for shellcheck. Planning to lean more heavily on LSP.
--
--  -- Auto-completion
--   use {
--     "hrsh7th/nvim-compe",
--     event = "InsertEnter",
--     config = function() require('config.compe') end,
--   }
--
--
--  -- Treesitter highlighting tangles with Vim's spell checking; this fixes it.
--  use {
--    'lewis6991/spellsitter.nvim',
--    config = function() require('spellsitter').setup() end,
--  }
--
--  -- Automatically generate and update a ctags file for the current project
--  use {
--    'ludovicchabant/vim-gutentags',
--    config = function()
--      vim.g.gutentags_ctags_tagfile = '.tags'
--      vim.g.gutentags_file_list_command = 'rg --files'
--    end,
--  }
--
--  use {
--    'chriskempson/base16-vim',
--    config = function() vim.cmd('colorscheme base16-summerfruit-dark') end,
--  }
--
--  -- Swap between single- and multi-line formats for lists, function arguments, etc.
--  use {
--    'FooSoft/vim-argwrap',
--    config = function()
--      vim.api.nvim_set_keymap('n', '<leader>a', ':ArgWrap<CR>', {noremap = true})
--    end,
--  }
--
--
--  -- The notorious Git plugin
--  use 'tpope/vim-fugitive'
--
--  -- Commands for adding, modifying, and deleting pairs of quotes, brackets, etc.
--  use 'tpope/vim-surround'
--
--
--
--
--  use {
--    'nathanaelkane/vim-indent-guides',
--    cmd = 'IndentGuidesToggle',
--    config = function()
--      vim.g.indent_guides_start_level = 2
--      vim.g.indent_guides_guide_size = 1
--      vim.api.nvim_set_keymap('n', '<leader>ig', ':IndentGuidesToggle<CR>', {noremap = true})
--    end
--  }

end)
